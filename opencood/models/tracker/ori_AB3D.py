#####AB3D代码
import pandas as pd#for test
import matplotlib; matplotlib.use('Agg')
import os.path, copy, numpy as np, time, sys
from numba import jit
from sklearn.utils.linear_assignment_ import linear_assignment # scikit-learn==0.19.2

from filterpy.kalman import KalmanFilter
#from scipy.optimize import linear_sum_assignment as linear_assignment#新版本api换了
from pcdet.models.tracker.utils import load_list_from_folder, fileparts, mkdir_if_missing
from scipy.spatial import ConvexHull
from .covariance import Covariance

import torch
import torch.nn.functional as F

def sigmoid(x):
    s = 1 / (1 + np.exp(-x))
    return s

####config parameter
#ifdoreid = True
reidlikthr = 0.5
use_mahalanobis = True          # standford is True   ab3d is false
use_angular_velocity = True     # standford is True   ab3d is false
covariance_id  = 2              #0 or 2    standford is 2          ab3d is 0
match_algorithm  = 'pre_threshold'   #'greedy' or 'pre_threshold' or None  or "reid-greedy" standford is greedy   ab3d is None

mahalanobis_threshold = 11  #need to test       standford is 11





NUSCENES_TRACKING_NAMES = ['car','truck', 'construction_vehicle', 'bus', 'trailer',
              'barrier', 'motorcycle', 'bicycle', 'pedestrian', 'traffic_cone']

trk_NUSCENES_TRACKING_NAMES = [
  'bicycle',
  'bus',
  'car',
  'motorcycle',
  'pedestrian',
  'trailer',
  'truck'
]
 

@jit    
def poly_area(x,y):
    return 0.5*np.abs(np.dot(x,np.roll(y,1))-np.dot(y,np.roll(x,1)))

@jit        
def box3d_vol(corners):
    ''' corners: (8,3) no assumption on axis direction '''
    a = np.sqrt(np.sum((corners[0,:] - corners[1,:])**2))
    b = np.sqrt(np.sum((corners[1,:] - corners[2,:])**2))
    c = np.sqrt(np.sum((corners[0,:] - corners[4,:])**2))
    return a*b*c

@jit       
def convex_hull_intersection(p1, p2):
    """ Compute area of two convex hull's intersection area.
        p1,p2 are a list of (x,y) tuples of hull vertices.
        return a list of (x,y) for the intersection and its volume
    """
    inter_p = polygon_clip(p1,p2)
    if inter_p is not None:
        hull_inter = ConvexHull(inter_p)
        return inter_p, hull_inter.volume
    else:
        return None, 0.0  

def polygon_clip(subjectPolygon, clipPolygon):
   """ Clip a polygon with another polygon.
   Args:
     subjectPolygon: a list of (x,y) 2d points, any polygon.
     clipPolygon: a list of (x,y) 2d points, has to be *convex*
   Note:
     **points have to be counter-clockwise ordered**

   Return:
     a list of (x,y) vertex point for the intersection polygon.
   """
   def inside(p):
      return(cp2[0]-cp1[0])*(p[1]-cp1[1]) > (cp2[1]-cp1[1])*(p[0]-cp1[0])
 
   def computeIntersection():
      dc = [ cp1[0] - cp2[0], cp1[1] - cp2[1] ]
      dp = [ s[0] - e[0], s[1] - e[1] ]
      n1 = cp1[0] * cp2[1] - cp1[1] * cp2[0]
      n2 = s[0] * e[1] - s[1] * e[0] 
      n3 = 1.0 / (dc[0] * dp[1] - dc[1] * dp[0])
      return [(n1*dp[0] - n2*dc[0]) * n3, (n1*dp[1] - n2*dc[1]) * n3]
 
   outputList = subjectPolygon
   cp1 = clipPolygon[-1]
 
   for clipVertex in clipPolygon:
      cp2 = clipVertex
      inputList = outputList
      outputList = []
      s = inputList[-1]
 
      for subjectVertex in inputList:
         e = subjectVertex
         if inside(e):
            if not inside(s):
               outputList.append(computeIntersection())
            outputList.append(e)
         elif inside(s):
            outputList.append(computeIntersection())
         s = e
      cp1 = cp2
      if len(outputList) == 0:
          return None
   return(outputList)

def iou3d(corners1, corners2):
    ''' Compute 3D bounding box IoU.

    Input:
        corners1: numpy array (8,3), assume up direction is negative Y
        corners2: numpy array (8,3), assume up direction is negative Y
    Output:
        iou: 3D bounding box IoU
        iou_2d: bird's eye view 2D bounding box IoU

    '''
    # corner points are in counter clockwise order
    rect1 = [(corners1[i,0], corners1[i,2]) for i in range(3,-1,-1)]
    rect2 = [(corners2[i,0], corners2[i,2]) for i in range(3,-1,-1)] 
    area1 = poly_area(np.array(rect1)[:,0], np.array(rect1)[:,1])
    area2 = poly_area(np.array(rect2)[:,0], np.array(rect2)[:,1])
    inter, inter_area = convex_hull_intersection(rect1, rect2)
    iou_2d = inter_area/(area1+area2-inter_area)
    ymax = min(corners1[0,1], corners2[0,1])
    ymin = max(corners1[4,1], corners2[4,1])
    inter_vol = inter_area * max(0.0, ymax-ymin)
    vol1 = box3d_vol(corners1)
    vol2 = box3d_vol(corners2)
    iou = inter_vol / (vol1 + vol2 - inter_vol)
    return iou, iou_2d

@jit       
def roty(t):
    ''' Rotation about the y-axis. '''
    c = np.cos(t)
    s = np.sin(t)
    return np.array([[c,  0,  s],
                     [0,  1,  0],
                     [-s, 0,  c]])

def convert_3dbox_to_8corner(bbox3d_input):
    ''' Takes an object and a projection matrix (P) and projects the 3d
        bounding box into the image plane.
        Returns:
            corners_2d: (8,2) array in left image coord.
            corners_3d: (8,3) array in in rect camera coord.
    '''
    # compute rotational matrix around yaw axis
    bbox3d = copy.copy(bbox3d_input)

    R = roty(bbox3d[3])    

    # 3d bounding box dimensions
    l = bbox3d[4]
    w = bbox3d[5]
    h = bbox3d[6]
    
    # 3d bounding box corners
    x_corners = [l/2,l/2,-l/2,-l/2,l/2,l/2,-l/2,-l/2];
    y_corners = [0,0,0,0,-h,-h,-h,-h];
    z_corners = [w/2,-w/2,-w/2,w/2,w/2,-w/2,-w/2,w/2];
    
    # rotate and translate 3d bounding box
    corners_3d = np.dot(R, np.vstack([x_corners,y_corners,z_corners]))
    corners_3d[0,:] = corners_3d[0,:] + bbox3d[0]
    corners_3d[1,:] = corners_3d[1,:] + bbox3d[1]
    corners_3d[2,:] = corners_3d[2,:] + bbox3d[2]
 
    return np.transpose(corners_3d)


##for 计算reid特征的余弦相似度或其他方法
def bit_product_sum(x, y):
    return sum([item[0] * item[1] for item in zip(x, y)])


def cosine_similarity(x, y, norm=False):
    """ 
    计算两个向量x和y的余弦相似度 
    用法
    print cosine_similarity([3, 3], [4, 4])  # 1.0
    print cosine_similarity([1, 2, 2, 1, 1, 1, 0], [1, 2, 2, 1, 1, 2, 1])  # 0.938194187433

    余弦值的范围在[-1,1]之间，值越趋近于1，代表两个向量的方向越接近；越趋近于-1，他们的方向越相反；接近于0，表示两个向量近乎于正交。
    一般情况下，相似度都是归一化到[0,1]区间内
    """
    assert len(x) == len(y), "dereid_len(x) != trkreid_len(y)"
    zero_list = [0] * len(x)
    if x == zero_list or y == zero_list:
        return float(1) if x == y else float(0)

    # method 1
    res = np.array([[x[i] * y[i], x[i] * x[i], y[i] * y[i]] for i in range(len(x))])
    cos = sum(res[:, 0]) / (np.sqrt(sum(res[:, 1])) * np.sqrt(sum(res[:, 2])))

    # method 2
    # cos = bit_product_sum(x, y) / (np.sqrt(bit_product_sum(x, x)) * np.sqrt(bit_product_sum(y, y)))

    # method 3
    # dot_product, square_sum_x, square_sum_y = 0, 0, 0
    # for i in range(len(x)):
    #     dot_product += x[i] * y[i]
    #     square_sum_x += x[i] * x[i]
    #     square_sum_y += y[i] * y[i]
    # cos = dot_product / (np.sqrt(square_sum_x) * np.sqrt(square_sum_y))

    return 0.5 * cos + 0.5 if norm else cos  # 归一化到[0, 1]区间内


class KalmanBoxTracker(object):
  """
  This class represents the internel state of individual tracked objects observed as bbox.
  """
  count = 0
  def __init__(self, bbox3D, info,reid=None, tracking_name='car', ):
    """
    Initialises a tracker using initial bounding box.
    """
    #define constant velocity model
    if not use_angular_velocity:
      self.kf = KalmanFilter(dim_x=10, dim_z=7)       
      self.kf.F = np.array([[1,0,0,0,0,0,0,1,0,0],      # state transition matrix
                            [0,1,0,0,0,0,0,0,1,0],
                            [0,0,1,0,0,0,0,0,0,1],
                            [0,0,0,1,0,0,0,0,0,0],  
                            [0,0,0,0,1,0,0,0,0,0],
                            [0,0,0,0,0,1,0,0,0,0],
                            [0,0,0,0,0,0,1,0,0,0],
                            [0,0,0,0,0,0,0,1,0,0],
                            [0,0,0,0,0,0,0,0,1,0],
                            [0,0,0,0,0,0,0,0,0,1]])     
      
      self.kf.H = np.array([[1,0,0,0,0,0,0,0,0,0],      # measurement function,
                            [0,1,0,0,0,0,0,0,0,0],
                            [0,0,1,0,0,0,0,0,0,0],
                            [0,0,0,1,0,0,0,0,0,0],
                            [0,0,0,0,1,0,0,0,0,0],
                            [0,0,0,0,0,1,0,0,0,0],
                            [0,0,0,0,0,0,1,0,0,0]])

    else:
      # with angular velocity
      self.kf = KalmanFilter(dim_x=11, dim_z=7)       
      self.kf.F = np.array([[1,0,0,0,0,0,0,1,0,0,0],      # state transition matrix
                            [0,1,0,0,0,0,0,0,1,0,0],
                            [0,0,1,0,0,0,0,0,0,1,0],
                            [0,0,0,1,0,0,0,0,0,0,1],  
                            [0,0,0,0,1,0,0,0,0,0,0],
                            [0,0,0,0,0,1,0,0,0,0,0],
                            [0,0,0,0,0,0,1,0,0,0,0],
                            [0,0,0,0,0,0,0,1,0,0,0],
                            [0,0,0,0,0,0,0,0,1,0,0],
                            [0,0,0,0,0,0,0,0,0,1,0],
                            [0,0,0,0,0,0,0,0,0,0,1]])     
      
      self.kf.H = np.array([[1,0,0,0,0,0,0,0,0,0,0],      # measurement function,
                            [0,1,0,0,0,0,0,0,0,0,0],
                            [0,0,1,0,0,0,0,0,0,0,0],
                            [0,0,0,1,0,0,0,0,0,0,0],
                            [0,0,0,0,1,0,0,0,0,0,0],
                            [0,0,0,0,0,1,0,0,0,0,0],
                            [0,0,0,0,0,0,1,0,0,0,0]])


    if covariance_id == 0: # exactly the same as AB3DMOT baseline
      # self.kf.R[0:,0:] *= 10.   # measurement uncertainty
      self.kf.P[7:,7:] *= 1000. #state uncertainty, give high uncertainty to the unobservable initial velocities, covariance matrix
      self.kf.P *= 10.
    
      # self.kf.Q[-1,-1] *= 0.01    # process uncertainty
      self.kf.Q[7:,7:] *= 0.01

    elif covariance_id == 1: # for kitti car, not supported
      covariance = Covariance(covariance_id)
      self.kf.P = covariance.P
      self.kf.Q = covariance.Q
      self.kf.R = covariance.R
    elif covariance_id == 2: # for nuscenes
      covariance = Covariance(covariance_id)
      self.kf.P = covariance.P[tracking_name]    #协方差为预设的
      self.kf.Q = covariance.Q[tracking_name]
      self.kf.R = covariance.R[tracking_name]
      if not use_angular_velocity:
        self.kf.P = self.kf.P[:-1,:-1]
        self.kf.Q = self.kf.Q[:-1,:-1]
    else:
      assert(False),"covariance_id have None"
      
    self.kf.x[:7] = bbox3D.reshape((7, 1))  #其他值为0，初始速度为0  初始观测值作为初始状态

    self.time_since_update = 0
    self.id = KalmanBoxTracker.count
    KalmanBoxTracker.count += 1
    self.history = []
    self.hits = 1           # number of total hits including the first detection
    self.hit_streak = 1     # number of continuing hit considering the first detection
    self.first_continuing_hit = 1
    self.still_first = True
    self.age = 0
    self.info = info        # other info
    self.reid = reid  #reid from detection
    self.tracking_name = tracking_name
    self.use_angular_velocity = use_angular_velocity

  def update(self, bbox3D,info,reid=None): 
    """ 
    Updates the state vector with observed bbox.
    """
    self.time_since_update = 0
    self.history = []
    self.hits += 1
    self.hit_streak += 1          # number of continuing hit
    if self.still_first:
      self.first_continuing_hit += 1      # number of continuing hit in the fist time
    
    ######################### orientation correction
    if self.kf.x[3] >= np.pi: self.kf.x[3] -= np.pi * 2    # make the theta still in the range
    if self.kf.x[3] < -np.pi: self.kf.x[3] += np.pi * 2

    new_theta = bbox3D[3]
    if new_theta >= np.pi: new_theta -= np.pi * 2    # make the theta still in the range
    if new_theta < -np.pi: new_theta += np.pi * 2
    bbox3D[3] = new_theta

    predicted_theta = self.kf.x[3]
    if abs(new_theta - predicted_theta) > np.pi / 2.0 and abs(new_theta - predicted_theta) < np.pi * 3 / 2.0:     # if the angle of two theta is not acute angle
      self.kf.x[3] += np.pi       
      if self.kf.x[3] > np.pi: self.kf.x[3] -= np.pi * 2    # make the theta still in the range
      if self.kf.x[3] < -np.pi: self.kf.x[3] += np.pi * 2
      
    # now the angle is acute: < 90 or > 270, convert the case of > 270 to < 90
    if abs(new_theta - self.kf.x[3]) >= np.pi * 3 / 2.0:
      if new_theta > 0: self.kf.x[3] += np.pi * 2
      else: self.kf.x[3] -= np.pi * 2
    
    ######################### 

    self.kf.update(bbox3D)  #bbox3D shape(7,)

    if self.kf.x[3] >= np.pi: self.kf.x[3] -= np.pi * 2    # make the theta still in the range
    if self.kf.x[3] < -np.pi: self.kf.x[3] += np.pi * 2
    self.info = info
    self.reid = reid #更新reid
  def predict(self):       
    """
    Advances the state vector and returns the predicted bounding box estimate.
    """
    self.kf.predict()      
    if self.kf.x[3] >= np.pi: self.kf.x[3] -= np.pi * 2
    if self.kf.x[3] < -np.pi: self.kf.x[3] += np.pi * 2

    self.age += 1
    if(self.time_since_update>0):
      self.hit_streak = 0
      self.still_first = False
    self.time_since_update += 1
    self.history.append(self.kf.x)
    return self.history[-1]

  def get_state(self):
    """
    Returns the current bounding box estimate.
    """
    return self.kf.x[:7].reshape((7, ))


def angle_in_range(angle):
  '''
  Input angle: -2pi ~ 2pi
  Output angle: -pi ~ pi
  '''
  if angle > np.pi:
    angle -= 2 * np.pi
  if angle < -np.pi:
    angle += 2 * np.pi
  return angle

def diff_orientation_correction(det, trk):
  '''
  return the angle diff = det - trk
  if angle diff > 90 or < -90, rotate trk and update the angle diff
  '''
  diff = det - trk
  diff = angle_in_range(diff)
  if diff > np.pi / 2:
    diff -= np.pi
  if diff < -np.pi / 2:
    diff += np.pi
  diff = angle_in_range(diff)
  return diff

def greedy_match(distance_matrix):
  '''
  Find the one-to-one matching using greedy allgorithm choosing small distance
  distance_matrix: (num_detections, num_tracks)
  '''
  matched_indices = []

  num_detections, num_tracks = distance_matrix.shape
  distance_1d = distance_matrix.reshape(-1)
  index_1d = np.argsort(distance_1d)
  index_2d = np.stack([index_1d // num_tracks, index_1d % num_tracks], axis=1)
  detection_id_matches_to_tracking_id = [-1] * num_detections
  tracking_id_matches_to_detection_id = [-1] * num_tracks
  for sort_i in range(index_2d.shape[0]):
    detection_id = int(index_2d[sort_i][0])
    tracking_id = int(index_2d[sort_i][1])
    if tracking_id_matches_to_detection_id[tracking_id] == -1 and detection_id_matches_to_tracking_id[detection_id] == -1:
      tracking_id_matches_to_detection_id[tracking_id] = detection_id
      detection_id_matches_to_tracking_id[detection_id] = tracking_id
      matched_indices.append([detection_id, tracking_id])

  matched_indices = np.array(matched_indices)
  return matched_indices


def associate_detections_to_trackers(detections,trackers,doreid=False,det_reid=None,trk_reid=None,iou_threshold=0.01,det_info=None,trk_info=None,trks_S=None,dets = None, trks=None,tracking_name= None):     
# def associate_detections_to_trackers(detections,trackers,iou_threshold=0.1):      # ablation study
# def associate_detections_to_trackers(detections,trackers,iou_threshold=0.25):
  """
  Assigns detections to tracked object (both represented as bounding boxes)

  detections:  N x 8 x 3
  trackers:    M x 8 x 3

  dets: N x 7
  trks: M x 7
  trks_S: N x 7 x 7

  det_reid=detections+reid,trk_reid=trackers+reid
  Returns 3 lists of matches, unmatched_detections and unmatched_trackers
  """
  if(len(trackers)==0):
    return np.empty((0,2),dtype=int), np.arange(len(detections)), np.empty((0,8,3),dtype=int)    
  
  iou_matrix = np.zeros((len(detections),len(trackers)),dtype=np.float32)
  reid_matrix = np.zeros((len(detections),len(trackers)),dtype=np.float32)
  cls_matrix = np.zeros((len(detections),len(trackers)),dtype=np.float32)
  distance_matrix = np.zeros((len(detections),len(trackers)),dtype=np.float32)

  #print(det_info,trk_info)
  #print(type(det_reid[:,-1]),type( trk_reid[:,-1]))#<class 'numpy.ndarray'> <class 'numpy.ndarray'>
  #print(len(det_reid[:,-1]), trk_reid[:,-1][0].shape)#104 torch.Size([128])
  if use_mahalanobis:
    assert(dets is not None)
    assert(trks is not None)
    assert(trks_S is not None)
  

  if doreid:
    #这样比for 循环快了很多

    reid_matrix = torch.matmul(F.normalize(torch.stack(list(det_reid[:,-1]), 0), p=2, dim=1), F.normalize(torch.stack(list(trk_reid[:,-1]),0), p=2, dim=1).t()).cpu().numpy()
    #print(reid_matrix)
    # for d,det in enumerate(det_reid):
    #   for t,trk in enumerate(trk_reid):
    #     #print(det[-1],trk[-1])# for test ly test ok
    #     #iou_matrix[d,t] = cosine_similarity(list(det[-1].cpu()),list(trk[-1].cpu()))  #for only reid
    #     iou_matrix[d,t] = torch.matmul(det[-1],trk[-1]).cpu()  #for only reid
    #print(iou_matrix)
    for d,det in enumerate(detections):
      for t,trk in enumerate(trackers):
        if use_mahalanobis:
          S_inv = np.linalg.inv(trks_S[t]) # 7 x 7  矩阵的逆
          diff = np.expand_dims(dets[d] - trks[t], axis=1) # 7 x 1
          # manual reversed angle by 180 when diff > 90 or < -90 degree
          corrected_angle_diff = diff_orientation_correction(dets[d][3], trks[t][3])
          diff[3] = corrected_angle_diff
          distance_matrix[d, t] = np.sqrt(np.matmul(np.matmul(diff.T, S_inv), diff)[0][0]) 
          
          if NUSCENES_TRACKING_NAMES[int(det_info[d])-1] == tracking_name or NUSCENES_TRACKING_NAMES[int(trk_info[t][0])-1] == tracking_name:
            if det_info[d] == trk_info[t][0] :
              cls_matrix[d,t] = 0.5
            else:
              cls_matrix[d,t] = -2.1  
          else:
            cls_matrix[d,t] = -2.1  
        else:
          iou_matrix[d,t] = iou3d(det,trk)[0]
          if NUSCENES_TRACKING_NAMES[int(det_info[d])-1] == tracking_name or NUSCENES_TRACKING_NAMES[int(trk_info[t][0])-1] == tracking_name:
            if det_info[d] == trk_info[t][0] :
              cls_matrix[d,t] = 0.5
            else:
              cls_matrix[d,t] = -2.1  
          else:
            cls_matrix[d,t] = -2.1  
    if use_mahalanobis:
      iou_matrix = -distance_matrix / mahalanobis_threshold   + 1.0                  # to 0-1  距离近的值变大   
      #print(iou_matrix[iou_matrix>0])
      iou_matrix[iou_matrix>0] = np.exp(4*(iou_matrix[iou_matrix>0]-1))              #  0-1  距离近的值变大    经过一个函数exp函数 
      #print(iou_matrix[iou_matrix>0])     
    #iou_matrix = ( iou_matrix + cls_matrix ) / 1.5 #ablation study        mashi+class
    #iou_matrix = (reid_matrix + iou_matrix) / 2.0 #ablation study        
    iou_matrix = (reid_matrix + iou_matrix + cls_matrix ) / 2.5
  else:
    for d,det in enumerate(detections):
      for t,trk in enumerate(trackers):
          if use_mahalanobis:
            S_inv = np.linalg.inv(trks_S[t]) # 7 x 7
            diff = np.expand_dims(dets[d] - trks[t], axis=1) # 7 x 1
            # manual reversed angle by 180 when diff > 90 or < -90 degree
            corrected_angle_diff = diff_orientation_correction(dets[d][3], trks[t][3])
            diff[3] = corrected_angle_diff
            distance_matrix[d, t] = np.sqrt(np.matmul(np.matmul(diff.T, S_inv), diff)[0][0]) 
            iou_matrix = -distance_matrix / mahalanobis_threshold   + 1.0            #可以用某个函数过滤一下
          else:
            iou_matrix[d,t] = iou3d(det,trk)[0]             # det: 8 x 3, trk: 8 x 3
  if match_algorithm == 'greedy':
    matched_indices = greedy_match(distance_matrix) 
    to_max_mask = distance_matrix > mahalanobis_threshold  # mahalanobis 按阈值过滤 
    iou_matrix[to_max_mask] = 0  
  elif match_algorithm == 'pre_threshold':
    if use_mahalanobis:
      to_max_mask = distance_matrix > mahalanobis_threshold  # mahalanobis 优先选取     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!可以给成负无穷！
      iou_matrix[to_max_mask] = 0
      to_max_mask = iou_matrix < iou_threshold    # iou 阈值再次过滤
      distance_matrix[to_max_mask] = 0
      iou_matrix[to_max_mask] = 0
    else:
      to_max_mask = iou_matrix < iou_threshold
      distance_matrix[to_max_mask] = 0
      iou_matrix[to_max_mask] = 0
    matched_indices = linear_assignment(-iou_matrix)      # houngarian algorithm
  elif match_algorithm == 'reid-greedy':
    matched_indices = greedy_match(-iou_matrix) 
    to_max_mask = distance_matrix > mahalanobis_threshold  # mahalanobis 按阈值过滤 
    iou_matrix[to_max_mask] = 0  
  else:
    matched_indices = linear_assignment(-iou_matrix)      # houngarian algorithm
  

  unmatched_detections = []
  for d,det in enumerate(detections):
    if(d not in matched_indices[:,0]):
      unmatched_detections.append(d)
  unmatched_trackers = []
  for t,trk in enumerate(trackers):
    if(t not in matched_indices[:,1]):
      unmatched_trackers.append(t)
  #mahalanobis_threshold = -2
  #filter out matched with low IOU
  matches = []
  for m in matched_indices:
    # if use_mahalanobis:
    #   if(iou_matrix[m[0],m[1]]<mahalanobis_threshold):#  when doreid ,iou_threshold is reid 阈值，reid cost在[0,1] 
    #     unmatched_detections.append(m[0])
    #     unmatched_trackers.append(m[1])
    #   else:
    #     matches.append(m.reshape(1,2))

    # else:
    if(iou_matrix[m[0],m[1]]<iou_threshold):#  when doreid ,iou_threshold is reid 阈值，reid cost在[0,1] 
      unmatched_detections.append(m[0])
      unmatched_trackers.append(m[1])
    else:
      matches.append(m.reshape(1,2))
  if(len(matches)==0):
    matches = np.empty((0,2),dtype=int)
  else:
    matches = np.concatenate(matches,axis=0)

  return matches, np.array(unmatched_detections), np.array(unmatched_trackers)

class AB3DMOT(object):
  def __init__(self,max_age=2,min_hits=3, tracking_name='car', use_angular_velocity=False,):     # max age will preserve the bbox does not appear no more than 2 frames, interpolate the detection
  # def __init__(self,max_age=3,min_hits=3):        # ablation study
  # def __init__(self,max_age=1,min_hits=3):      
  # def __init__(self,max_age=2,min_hits=1):      
  # def __init__(self,max_age=2,min_hits=5):      
    """              
    """
    self.max_age = max_age
    self.min_hits = min_hits
    self.trackers = []
    self.frame_count = 0
    self.reorder = [3, 4, 5, 6, 2, 1, 0]
    self.reorder_back = [6, 5, 4, 0, 1, 2, 3]
    self.tracking_name = tracking_name
    self.use_angular_velocity = use_angular_velocity   #theta 用来改变旋转角

  def update(self,dets_all,doreid=False):#ly
    """
    Params:
      dets_all: dict
        dets - a numpy array of detections in the format [[x,y,z,theta,l,w,h],[x,y,z,theta,l,w,h],...]
        info: a array of other info for each det
    Requires: this method must be called once for each frame even with empty detections.
    Returns the a similar array, where the last column is the object ID.

    NOTE: The number of objects returned may differ from the number of detections provided.
    """
    dets, info, det_reids = dets_all['dets'], dets_all['info'] ,dets_all['det_reids']        # dets: N x 7, float numpy array
    
    
    #det_reids是dets+相应的reid特征
    ## 将相应的reid feature  写至文件
    

    dets = dets[:, self.reorder]
    self.frame_count += 1

    trks = np.zeros((len(self.trackers),7))         # N x 7 ,当前跟踪器跟踪的N个物体 ,所以是 #get predicted locations from existing trackers.
    trk_reid = np.zeros((len(self.trackers),8),dtype=object)   #N x 8 ，trks+reid，dtype=object为了使array里能有其他array或str等多种元素， ly
    trk_info = np.zeros((len(self.trackers),1),dtype=object)   #给同类别关联所用
    to_del = []
    ret = []

#对当前帧 用跟踪器跟踪的轨迹做出kf预测，并且提取当前跟踪器内容
    
    for t,trk in enumerate(trks):
      pos = self.trackers[t].predict().reshape((-1, 1))
      trk[:] = [pos[0], pos[1], pos[2], pos[3], pos[4], pos[5], pos[6]]       
      trk_reid[t] =[pos[0], pos[1], pos[2], pos[3], pos[4], pos[5], pos[6],self.trackers[t].reid]#ly trackingreid
      trk_info[t] = [self.trackers[t].info[1]]
      #print(trk[:]) #输出一个维度的
      
      #if(np.any(np.isnan(pos))):
      if(pd.isnull(np.isnan(pos.all()))):#ly
        to_del.append(t)
    trks = np.ma.compress_rows(np.ma.masked_invalid(trks))  #把各维度的值组合成一个整的array
    #trk_reid = np.ma.compress_rows(np.ma.masked_invalid(trk_reid)) #ly 经test和上边对齐了
    #print('current tracing reid')
    #print(trk_reid)
    # print('current tracing trks')
    # print(trks)
    for t in reversed(to_del):
      self.trackers.pop(t)

    dets_8corner = [convert_3dbox_to_8corner(det_tmp) for det_tmp in dets]
    # if doreid:
    #   for i in range(len(dets)):
    #     print (dets_8corner[i],det_reids[i])
    #     #np.concatenate(dets_8corner[i],det_reids[i])
    # print(dets_8corner)#ly
    if len(dets_8corner) > 0: 
      dets_8corner = np.stack(dets_8corner, axis=0)
    else: 
      dets_8corner = []
    
    trks_8corner = [convert_3dbox_to_8corner(trk_tmp) for trk_tmp in trks]
    trks_S = [np.matmul(np.matmul(tracker.kf.H, tracker.kf.P), tracker.kf.H.T) + tracker.kf.R for tracker in self.trackers]
    if len(trks_8corner) > 0: 
      trks_8corner = np.stack(trks_8corner, axis=0)
      trks_S = np.stack(trks_S, axis=0)
    
#数据关联    
    for clas in trk_NUSCENES_TRACKING_NAMES:
      if doreid:# 为先位置关联再reid关联
        
        
        #先reid
        matched, unmatched_dets, unmatched_trks = associate_detections_to_trackers(\
                                        dets_8corner, trks_8corner,\
                                        det_reid=det_reids,trk_reid=trk_reid,\
                                        doreid=doreid,iou_threshold=reidlikthr,det_info=info[:,1],trk_info=trk_info,trks_S=trks_S, dets=dets, trks=trks, tracking_name= clas)  #测出来reid单独最好值在0.7处
        

        '''
        matched, unmatched_dets, unmatched_trks= associate_detections_to_trackers(\
                                            dets_8corner, trks_8corner,
                                            )#原 IOU先过一遍
        #print( matched, unmatched_dets, unmatched_trks)
        '''

        '''
        if len(unmatched_dets)==0 or len(unmatched_trks)==0:
          pass
        else:
        
        '''

          
          # #再iou
          # re_matched, re_unmatched_dets, re_unmatched_trks= associate_detections_to_trackers(\
          #                                    dets_8corner[unmatched_dets,:,:], trks_8corner[unmatched_trks,:,:],
          #                                    )   #原 IOU先过一遍
          

          
          # #再reid
          # re_matched, re_unmatched_dets, re_unmatched_trks = associate_detections_to_trackers(\
          #                               dets_8corner[unmatched_dets,:,:], trks_8corner[unmatched_trks,:,:],\
          #                               det_reid=det_reids[unmatched_dets,:],trk_reid=trk_reid[unmatched_trks,:],\
          #                               doreid=doreid,iou_threshold=0.58) 
            

          #过滤未匹配
          # for x in re_matched:
          #   #print(type(x),x)
          #   matched = np.vstack((matched,np.array([unmatched_dets[x[0]], unmatched_trks[x[1]]])))#合并【】【】
          # if len(re_unmatched_dets)>0:
          #   unmatched_dets = unmatched_dets[re_unmatched_dets]
          # else:
          #   unmatched_dets = re_unmatched_dets
          # if len(re_unmatched_trks)>0:
          #   unmatched_trks = unmatched_trks[re_unmatched_trks]
          # else:
          #   unmatched_trks = re_unmatched_trks
          
        
          #print( matched, unmatched_dets, unmatched_trks)
      else:
        matched, unmatched_dets, unmatched_trks = associate_detections_to_trackers(dets_8corner, trks_8corner)#原

  #跟踪管理    
      #update matched trackers with assigned detections
      for t,trk in enumerate(self.trackers):
        if NUSCENES_TRACKING_NAMES[int(trk.info[1])-1]== clas:
          if t not in unmatched_trks:
            d = matched[np.where(matched[:,1]==t)[0],0]     # a list of index
            #trk.update(dets[d,:][0], info[d, :][0])  
            #print(dets[d,:][0], info[d, :][0],det_reids[d,-1][0])
            trk.update(dets[d,:][0], info[d, :][0],reid=det_reids[d,-1][0])  #不知为何有[0]??根据当前detection更新

      #create and initialise new trackers for unmatched detections
      for i in unmatched_dets:        # a scalar of index
          #trk = KalmanBoxTracker(dets[i,:], info[i, :]) 
          if NUSCENES_TRACKING_NAMES[int(info[i][1])-1] == clas:
            trk = KalmanBoxTracker(dets[i,:], info[i, :], det_reids[i,-1],tracking_name=NUSCENES_TRACKING_NAMES[int(info[i,1])-1] )#ly 传入已跟踪对象的对应的reid
          
            self.trackers.append(trk)
      
      
      
    i = len(self.trackers)
      
    #print(self.trackers[:],self.trackers[:])
    #为输出tracking结果的格式，最后写文件的格式
    for trk in reversed(self.trackers):#reversed()函数是返回序列seq的反向访问的迭代器。反向对应上边的i
        #print(trk.id,trk.reid,trk)
        ##输出到文件同一辆车的trackid和reidfe
          ### reid for writing to results
        '''
        with open('../reidferes/sumcar2dthr1-npa-reneg1.txt','a') as feou:
          
          feou.write('%d,car,%s;' % (trk.id,trk.reid))
        '''
        d = trk.get_state()      # 得到当前所有跟踪对象的bbox location
        d = d[self.reorder_back] #更换成它给定的输出位置格式

        if((trk.time_since_update < self.max_age) and (trk.hits >= self.min_hits or self.frame_count <= self.min_hits)):      
          ret.append(np.concatenate((d, [trk.id+1], trk.info)).reshape(1,-1)) # +1 as MOT benchmark requires positive
        i -= 1
        #remove dead tracklet
        if(trk.time_since_update >= self.max_age):
          self.trackers.pop(i)
    if(len(ret)>0):
      return np.concatenate(ret)      # x, y, z, theta, l, w, h, ID, other info, confidence
    return np.empty((0,15)) 
#####上面是AB3D代码